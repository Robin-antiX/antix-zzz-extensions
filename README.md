# antiX-zzz-extensions

Some scripts providing additional context menu functionality for antiX zzz file manager (zzzFM).

## Usage

### Search Doublettes
This menu entry searches for doublette files in current folder and subdirectories, depending on currently selected files it knows four modes:
- If a single file is selected, »Search Doublettes« will search for a precise duplicate of the selected file in current directory and it's subdirectory tree.
- If a directory is selected, it will search all branches of the subdirectory tree for all doublettes of files.
- If nothing is selected, it will include to the directory search mode the current directory.

Doublettes are found independendly of file name or creation date of a file by their uniqe MD5 hashsum.
Applies currently to the active panel in zzzFM. Other visible panels are not searched.

### Compare with merge_when_pipeline_succeeds
This menu entry compares files or folders, even across the currently visible panes of zzzFM.
- If two or three files are selected, even across visible panes of zzzFM, these will be compared using meld for differences.
- If two or three folders are selected, even across visible panes of zzzFM, these will be compared recursively down the subfolder tree for different filenames and folder names. This folder comparison is (on contrary to the search doublettes entry) NOT done by a bitwise comparison using hashes.
- If nothing is selected, the currently visible panes will be compared in folder comparison mode.

You can use this for simple text file comparison, antiX config file version comparison, program config file comparison, comparison of antiXradio or aCSTV stations list files, but also for hex comparing of binaries and comparison of localisation files (.po or .pot type)

**Restrictions:**
- You can't compare more than three files or folders. Hence, if you have visible all four panes in zzzFM, the comparison will fail if nothing is explicitely selected.
- You can't compare files with folders, which should be evident.

## Installation
These scripts are tailored for the internal zzzFM scripting interface, making use of the zzzFM internal handlers. Thus they will fail when running on bash directly, since expected variables are not present. You may source the current temporary _f-tmp.sh_ file zzzFM creates in runtime to make them work on bash directly.

For manual installation please copy the script content to an internal zzzFM script tempate by clicking open the second layer context menu in zzzFM, adding a command, entering the desired menu entry name (e.g. either »_Search Doublettes_« or »_Compare with Meld_«), then selecting the method "script" and pasting the content. Make sure to have the settings in all the tabs of the creation dialog adjusted properly before giving OK to create the entry. Repeat the steps for all provided scripts in this package.

## Support
For help, questions, suggestions and bug reporting please write to [antiXlinux forums](https://www.antixforum.com).

## Contributing
- Translations to your language and fixes of wrongly translated texts can be done simply by visiting [antix-translations at transifex](https://app.transifex.com/anticapitalista/antix-development/antix-zzz-extensions/). Your improvements will get included into the next update then.
- Code improvements always welcome. Just post your proposals to [antiXlinux forums](https://www.antixforum.com) to discuss, or send a merge request here.

## Authors and acknowledgment
This is an antiX community project.

## License
GPL version 3

## Project status
active

-----------
Written 2024 by Robin.antiX for antiX community.
