��          t      �         '     #   9  <   ]  &   �     �  P   �     &  J   ,  Q   w  d   �  n  .  =   �  4   �  f     L   w  $   �  }   �     g  S   x  X   �  o   %                 
                       	                    %s doublettes with MD5-Hash '%s' found: Can‘t compare files with folders! Can‘t compare more than three files or folders using Meld! No doublettes for MD5-Hash '%s' found. No doublettes found Nothing to do. Comparing a single file or folder, or even less, is not possible! Oops! Searching for doublettes in current directory './' and its subdirectories: Searching for doublettes in directory './${fm_file##*/}'\nand its subdirectories: Searching for duplicates of file '${fm_filename}'\nin current directory './' and its subdirectories: Project-Id-Version: antix-zzz-extensions-0.1
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2024-02-06 19:51+0100
Last-Translator: 
Language-Team: antiX translation team
Language: de
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 3.2.2
X-Poedit-SourceCharset: UTF-8
 Es wurden %s Doubletten mit der MD5-Prüfsumme '%s' gefunden: Dateien können nicht mit Ordnern verglichen werden! Vergleich nicht möglich. Meld kann maximal drei \nDateien oder Verzeichnisse miteinander vergleichen. Für die Datei mit der MD5-Prüfsumme '%s' wurden keine Doubletten gefunden. Es wurden keine Doubletten gefunden. Vergleich nicht möglich. Es müssen wenigstens zwei Dateien\noder zwei Ordner ausgewählt werden, um vergleichen zu können. Nein! Doch. Ohh! Suche nach Doubletten im aktuellen Verzeichnis './' und seinen Unterverzeichnissen: Suche nach Doubletten im Verzeichnis './${fm_file##*/}'\nund seinen Unterverzeichnissen: Suche nach Doubletten der Datei '${fm_filename}'\nim aktuellen Verzeichnis './' und seinen Unterverzeichnissen: 